export class Car {
    vin:string;
    year:string;
    brand:string;
    color:string;
    /**
     *
     */
    constructor( vin?,
        year?,
        brand?,
        color?) {
        this.brand=brand
        this.color=color
        this.vin=vin
        this.year=year
        
    }
}