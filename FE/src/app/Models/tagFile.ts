

export class tagFile{
    id:number
    file_name:string
    file_size_bytes:number
    status:number
    Text:string
    single_tag_price:number
    file_price:number
    upload_date:Date
}