import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './Landing/login/login.component';
import { RegisterComponent } from './Landing/register/register.component';
import { ResetPasswordComponent } from './Landing/reset-password/reset-password.component';
import { FooterComponent } from './General/footer/footer.component';
import { ErrorPathComponent } from './General/error-path/error-path.component';
import { HeaderLandingComponent } from './Landing/header-landing/header-landing.component';
import { RouterModule } from '@angular/router';
import { LandingRoutes } from 'src/app/routes';
import {from} from 'rxjs';
import { ForgottenPasswordComponent } from './Landing/forgotten-password/forgotten-password.component';
import { LandingPageComponent } from './Landing/landing-page/landing-page.component';
import { TermsAndConditionsComponent } from 'src/app/Landing/terms-and-conditions/terms-and-conditions.component';
import { InstructionsComponent } from './Tagger/instructions/instructions.component';
import { TaggerEntranceComponent } from './Tagger/tagger-entrance/tagger-entrance.component';
import { TaggingComponent } from './Tagger/tagging/tagging.component';
import { HttpClientModule } from '@angular/common/http';
import { ProfileImageComponent } from './Landing/profile-image/profile-image.component';
import { LogOutComponent } from './General/log-out/log-out.component';
import { ContactUsComponent } from './Landing/contact-us/contact-us.component';
import { PaymentsComponent } from './Tagger/payments/payments.component';
import { StartPageComponent } from './Landing/start-page/start-page.component';
import { LandingTaggerComponent } from './Tagger/landing-tagger/landing-tagger.component';
import { LandingAdminComponent } from './Admin/landing-admin/landing-admin.component';
import { AdminEntranceComponent } from './Admin/admin-entrance/admin-entrance.component';
import { ManagePaymentsComponent } from './Admin/manage-payments/manage-payments.component';
import { ManageFilesComponent } from './Admin/manage-files/manage-files.component';
import { ManageTaggersComponent } from './Admin/manage-taggers/manage-taggers.component';
import { ManageMessagesComponent } from './Admin/manage-messages/manage-messages.component';
import { SidenavAdminComponent } from './Admin/sidenav-admin/sidenav-admin.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { SidenavTaggerComponent } from './Tagger/sidenav-tagger/sidenav-tagger.component';
import { NavbarAdminComponent } from './Admin/navbar-admin/navbar-admin.component';
import { NavbarTaggerComponent } from './Tagger/navbar-tagger/navbar-tagger.component';
import { HowToDoThatComponent } from './Landing/how-to-do-that/how-to-do-that.component';
import { AdminService } from './Services/admin.service';
import { AccountService } from './Services/account.service';
import { EmailService } from './Services/email.service';
import { TagFileService } from './Services/tag-file.service';
import { TagService } from './Services/tag.service';
import {DropdownModule} from 'primeng/dropdown';

// import {AccordionModule} from 'primeng/accordion';     //accordion and accordion tab
// import { MenuItem} from 'primeng/api';                 //api
// import {TableModule} from 'primeng/table';
// import { PanelModule } from 'primeng/primeng';
// import { ButtonModule } from 'primeng/primeng';
// import { RadioButtonModule } from 'primeng/primeng';
// import { Ng2SmartTableModule } from 'ng2-smart-table';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ResetPasswordComponent,
    FooterComponent,
    ErrorPathComponent,
    HeaderLandingComponent,
    ForgottenPasswordComponent,
    LandingPageComponent,
    TermsAndConditionsComponent,
    InstructionsComponent,
    TaggerEntranceComponent,
    TaggingComponent,
    ProfileImageComponent,
    LogOutComponent,
    ContactUsComponent,
    PaymentsComponent,
    StartPageComponent,
    LandingAdminComponent,
    AdminEntranceComponent,
    ManagePaymentsComponent,
    LandingTaggerComponent,
    ManageFilesComponent,
    ManageTaggersComponent,
    ManageMessagesComponent,
    SidenavAdminComponent,
    SidenavTaggerComponent,
    NavbarAdminComponent,
    NavbarTaggerComponent,
    HowToDoThatComponent,
    // Ng2SmartTableModule
 ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    CommonModule, 
    FormsModule,
    ReactiveFormsModule,
   DropdownModule,



    // AccordionModule,
    // MenuItem,
    // TableModule,

    BrowserAnimationsModule,
    // MatCheckboxModule,
    // MatButtonModule,
    // MatFormFieldModule,
    // MatButtonToggleModule,
    BrowserModule,
    // MatSidenavModule,
    ReactiveFormsModule,
    // MatAutocompleteModule,
    // MatSidenavModule,
    // MatTabsModule,
    // MatToolbarModule,
    // MatListModule,
    // LayoutModule,
    // DataTableModule,
   

    RouterModule.forRoot( LandingRoutes
    ),
    
  ],

  // I'm not sure that we really need it
  exports:[
    // Ng2SmartTableModule
  ], 

  providers: [AdminService,AccountService,EmailService,TagFileService,TagService],
  bootstrap: [AppComponent]
})
export class AppModule { }
