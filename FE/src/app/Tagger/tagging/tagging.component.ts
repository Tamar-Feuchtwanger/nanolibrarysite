import { Component, OnInit, Input } from '@angular/core';
import { TagFileService } from 'src/app/Services/tag-file.service';
import { tag } from 'src/app/Models/tag';
import { TagService } from 'src/app/Services/tag.service';
import { tagProperty } from 'src/app/Models/tagProperty';
import {SelectItem} from 'primeng/api';

@Component({
  selector: 'app-tagging',
  templateUrl: './tagging.component.html',
  styleUrls: ['./tagging.component.scss']
})
export class TaggingComponent implements OnInit {
  arr=[];
  temp: string="";
  flag:number=0;
  currentTag=new tag();
  //tagPropertyList:tagProperty[];
  selectedTagProperty:tagProperty;

  constructor(private tagFileServ: TagFileService,
              private tagServ:TagService) 
  { }

ngOnInit()
{
this.getNewFile();
this.getTagPropertyList();
}

getTagPropertyList()
{
  this.tagServ.getTagPropertyList().subscribe(
    data=>
    {
    this.tagServ.tagPropertyList=data;
    console.log(this.tagServ.tagPropertyList);
    },
    err=>
    {
    alert(err);
    });
  }

 getNewFile() {
    console.log('tagging component');
    this.tagFileServ.getFile().subscribe(
      data => {
        this.tagFileServ.currentFile = data;
        console.log(this.tagFileServ.currentFile.Text);
        this.splitFile();
      },
      error => {
       alert(error);
      });
  }

  splitFile() {
    this.arr=(this.tagFileServ.currentFile.Text||'').split(',');
  }

  value() {
    if(this.temp)
    {
    this.currentTag.value = this.temp;
    console.log(this.currentTag.value);
    this.flag++;
    this.temp="";
  }
  this.checking();
  }

  propertyWord() {
    if(this.temp)
    {
      this.currentTag.propertyWord = this.temp;
    console.log(this.currentTag.propertyWord);
      this.flag++;
      this.temp="";
    }
    this.checking();
  }

  propertyName() {
 console.log('property name');
    // this.currentTag.property_id = this.temp;
    this.flag++;
    this.temp="";
    
  }

  addTagToDB()
  {
    alert('flag is 3 now- add tag to db');
    this.tagServ.addNewTagToDB(this.currentTag).subscribe(
      data=>alert('Created Tag Successfully!'),
      error=>alert('cannot create tag'))
  }

  checking()
  {
    console.log('checking', this.flag);
    if(this.flag==3)
    {
      this.addTagToDB();
      this.flag=0;
    }
  }


}
