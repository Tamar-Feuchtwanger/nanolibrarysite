import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaggerEntranceComponent } from './tagger-entrance.component';

describe('TaggerEntranceComponent', () => {
  let component: TaggerEntranceComponent;
  let fixture: ComponentFixture<TaggerEntranceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaggerEntranceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaggerEntranceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
