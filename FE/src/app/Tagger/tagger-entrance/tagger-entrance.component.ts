import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TagFileService } from 'src/app/Services/tag-file.service';

@Component({
  selector: 'app-tagger-entrance',
  templateUrl: './tagger-entrance.component.html',
  styleUrls: ['./tagger-entrance.component.scss']
})
export class TaggerEntranceComponent implements OnInit {

  constructor(private router:Router, private tagFileServ:TagFileService) { }

  ngOnInit() {
  }

}
