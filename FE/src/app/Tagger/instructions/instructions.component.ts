import { Component, OnInit, Input } from '@angular/core';

import { Location } from '@angular/common';

@Component({
  selector: 'app-instructions',
  templateUrl: './instructions.component.html',
  styleUrls: ['./instructions.component.scss']
})
export class InstructionsComponent implements OnInit {


  constructor(private location: Location) {}

  goBack() {
    this.location.back();
  }   

  ngOnInit() {
  }

}
