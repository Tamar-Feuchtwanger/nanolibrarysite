import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarTaggerComponent } from './navbar-tagger.component';

describe('NavbarTaggerComponent', () => {
  let component: NavbarTaggerComponent;
  let fixture: ComponentFixture<NavbarTaggerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavbarTaggerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarTaggerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
