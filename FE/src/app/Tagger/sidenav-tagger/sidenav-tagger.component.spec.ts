import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidenavTaggerComponent } from './sidenav-tagger.component';

describe('SidenavTaggerComponent', () => {
  let component: SidenavTaggerComponent;
  let fixture: ComponentFixture<SidenavTaggerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidenavTaggerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidenavTaggerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
