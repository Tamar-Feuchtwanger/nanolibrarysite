import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingTaggerComponent } from './landing-tagger.component';

describe('LandingTaggerComponent', () => {
  let component: LandingTaggerComponent;
  let fixture: ComponentFixture<LandingTaggerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandingTaggerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingTaggerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
