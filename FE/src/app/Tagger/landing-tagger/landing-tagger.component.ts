import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { account } from 'src/app/Models/account';

@Component({
  selector: 'app-landing-tagger',
  templateUrl: './landing-tagger.component.html',
  styleUrls: ['./landing-tagger.component.scss']
})
export class LandingTaggerComponent implements OnInit {
  public showNotification: boolean;

  public href:string="";

  // currentUser=new account();
  // currentUser:account;
 
  constructor(public router:Router) {
    // this.showNotification = true;
    // this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }
  ngOnInit() {
    this.href=this.router.url;
  }

  // public onCloseClick(): void {
  //   this.showNotification = !this.showNotification;
  // }
}
