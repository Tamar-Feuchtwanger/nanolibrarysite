import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tagFile } from '../Models/tagFile';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TagFileService{
  
  BASIC_URL="http://localhost:53911/";
  currentFile:tagFile;
  
  cu:string;

  constructor(private http:HttpClient) { }

getFile():Observable<tagFile>
{
  return this.http.get<tagFile>(this.BASIC_URL+"api/TagFile/getFile")
}

postFile(fileToUpload: File): Observable<boolean> {
  const formData: FormData = new FormData();
  formData.append('fileKey', fileToUpload, fileToUpload.name);
  return this.http.post<boolean>(this.BASIC_URL+"api/TagFile",formData);
    // .post(endpoint, formData, { headers: yourHeadersConfig })
    // .map(() => { return true; })
    // .catch((e) => this.handleError(e));
}

}