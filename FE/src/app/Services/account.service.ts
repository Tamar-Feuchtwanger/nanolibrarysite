import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { account } from '../Models/account';
import { Observable } from 'rxjs';
import { contact } from '../Models/contact';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

BASIC_URL="http://localhost:53911/";
currentAccount=new account();
  
constructor(private http: HttpClient){}

register():Observable<account>
{
  return this.http.post<account>(this.BASIC_URL+"api/Account/register",this.currentAccount);
}

login():Observable<account>
{
  return this.http.put<account>(this.BASIC_URL+"api/Account/login",this.currentAccount);
}

forgottenPassword():Observable<account>
{
  this.currentAccount.email="tamarfoich@gmail.com";
  return this.http.put<account>(this.BASIC_URL+"api/Account/ForgottenPassword",this.currentAccount.email);
}

//function for update new password and login...
// resetPassword():Observable<account>
// {
//   console.log("reset password");
//   return this.http.put<account>(this.BASIC_URL+"api/ManageUser/resetPassword",this.currentAccount);
// }

// getAll() {
//   return this.http.get<User[]>(`${environment.apiUrl}/users`);
// }

// getById(id: number) {
//   return this.http.get(`${environment.apiUrl}/users/` + id);
// }

// update(user: User) {
//   return this.http.put(`${environment.apiUrl}/users/` + user.id, user);
// }

// delete(id: number) {
//   return this.http.delete(`${environment.apiUrl}/users/` + id);
// }


}
