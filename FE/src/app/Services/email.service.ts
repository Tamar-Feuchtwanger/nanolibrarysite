import { Injectable } from '@angular/core';
import { contact } from '../Models/contact';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class EmailService {

BASIC_URL="http://localhost:53911/";
currentContact=new contact();

constructor(private http:HttpClient) { }

  //a function for sending the user message 
  contact():Observable<contact>
  {
   return this.http.post<contact>(this.BASIC_URL+"api/ManageUser/contactUs",this.currentContact);
  }
  
}
