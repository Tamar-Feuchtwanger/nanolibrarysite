import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { account } from '../Models/account';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  BASIC_URL='http://localhost:53911/';
  
 


  constructor(private http: HttpClient) { }


  getAllTaggers():Observable<Array<account>>{
      return this.http.get<Array<account>>(this.BASIC_URL+"api/Admin/getAllAccounts");
  }
  removeAccount( id:any):Observable<Array<account>>{
    return this.http.get<Array<account>>(this.BASIC_URL+"api/Admin/removeAccount/{id}");
}

}
