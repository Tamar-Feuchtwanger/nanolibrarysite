import { TestBed } from '@angular/core/testing';

import { TagFileService } from './tag-file.service';

describe('TagFileService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TagFileService = TestBed.get(TagFileService);
    expect(service).toBeTruthy();
  });
});
