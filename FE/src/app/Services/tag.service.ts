import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tagProperty } from '../Models/tagProperty';
import { tag } from '../Models/tag';

@Injectable({
  providedIn: 'root'
})
export class TagService {

BASIC_URL="http://localhost:53911/";
tagPropertyList:tagProperty[];

constructor(private http:HttpClient) { }

getTagPropertyList():Observable<Array<tagProperty>>
{
return this.http.get<Array<tagProperty>>(this.BASIC_URL+"api/Tag/getTagPropertyList");
}

addNewTagToDB(currentTag:tag):Observable<tag>
{
return this.http.post<tag>(this.BASIC_URL+"api/Tag/create+currentTag.id",currentTag);
}

}
