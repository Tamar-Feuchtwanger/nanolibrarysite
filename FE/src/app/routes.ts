import { LoginComponent } from './Landing/login/login.component'
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { Route } from '@angular/compiler/src/core';
import { Routes } from '@angular/router';
import { RegisterComponent } from './Landing/register/register.component';
import { ForgottenPasswordComponent } from 'src/app/Landing/forgotten-password/forgotten-password.component';
import { LandingPageComponent } from 'src/app/Landing/landing-page/landing-page.component';
import { TaggerEntranceComponent } from './Tagger/tagger-entrance/tagger-entrance.component';
import { TaggingComponent } from './Tagger/tagging/tagging.component';
import { TermsAndConditionsComponent } from 'src/app/Landing/terms-and-conditions/terms-and-conditions.component';
import { ProfileImageComponent } from 'src/app/Landing/profile-image/profile-image.component';
import { InstructionsComponent } from './Tagger/instructions/instructions.component';
import { StartPageComponent } from 'src/app/Landing/start-page/start-page.component';
import { LandingTaggerComponent } from './Tagger/landing-tagger/landing-tagger.component';
import { PaymentsComponent } from './Tagger/payments/payments.component';
import { LogOutComponent } from './General/log-out/log-out.component';
import { LandingAdminComponent } from './Admin/landing-admin/landing-admin.component';
import { AdminEntranceComponent } from './Admin/admin-entrance/admin-entrance.component';
import { ManageFilesComponent } from './Admin/manage-files/manage-files.component';
import { ManageTaggersComponent } from './Admin/manage-taggers/manage-taggers.component';
import { ManagePaymentsComponent } from './Admin/manage-payments/manage-payments.component';
import { ResetPasswordComponent } from './Landing/reset-password/reset-password.component';
import { ManageMessagesComponent } from 'src/app/Admin/manage-messages/manage-messages.component';



export const LandingRoutes:Routes=
[
    { path:'',redirectTo:'/LandingPage/StartPage', pathMatch: 'full'},
    { path:'LandingPage',component:LandingPageComponent,
    children:
    [
        { path: 'StartPage',  component:StartPageComponent},
        { path: 'Login',  component:LoginComponent},
        { path: 'ForgottenPassword', component:ForgottenPasswordComponent}, 
        { path: 'Register', component:RegisterComponent},
        { path: 'ResetPassword',component:ResetPasswordComponent},
        { path: 'TermsAndConditions', component:TermsAndConditionsComponent},
        { path: 'ProfileImage', component:ProfileImageComponent}
    ]},
        {path: 'LandingTagger',component:LandingTaggerComponent,
    children:
    [
        { path: 'TaggerEntrance',  component:TaggerEntranceComponent},
        { path:'',redirectTo:'/LandingTagger/TaggerEntrance', pathMatch: 'full'},
        { path: 'Instructions',  component:InstructionsComponent},
        { path: 'Payments', component:  PaymentsComponent}, 
        { path: 'Tagging', component:TaggingComponent},
        
    ]},
    { path:'LandingAdmin', component:LandingAdminComponent,
    children:
    [
         { path:'' ,redirectTo:'/LandingAdmin/AdminEntrance', pathMatch: 'full'},
         { path: 'AdminEntrance',  component:AdminEntranceComponent},
         { path: 'ManageFiles',  component:ManageFilesComponent},
         { path: 'ManageTaggers',  component:ManageTaggersComponent},
         { path: 'ManagePayments',  component:ManagePaymentsComponent},
         { path: 'ManageMessages', component:ManageMessagesComponent }
      
    ]},
    { path:'LogOut', component:LogOutComponent}
  ]

