import { Component, OnInit } from '@angular/core';
import { contact } from 'src/app/Models/contact';
import { EmailService } from 'src/app/Services/email.service';
import { FormBuilder,FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {

  constructor(private emailServ:EmailService, private formBuilder: FormBuilder,) { }

//  contactForm: FormGroup;
  // loading = false;
  submitted = false;

  // convenience getter for easy access to form fields
  // get f() { return this.contactForm.controls; }

  //function when the user press 'submit' for sending his message
  sendContact()
  {
    // this.submitted = true;
    //stop here if form is invalid
    // if (this.contactForm.invalid)
    // {
    //     return;
    // }
    // this.loading = true;
    console.log(this.emailServ.currentContact);
    this.emailServ.contact().subscribe(
      data => {
       console.log("contact successful");
      //  this.router.navigate(['/LandingTagger']);
    },
      error => {
        console.log("cant contact");
        //  this.loading = false;
  });
}

    ngOnInit():void
    {
    //   this.contactForm = this.formBuilder.group({
    //     name: ['', Validators.required],
    //     email: ['', Validators.required,Validators.email],
    //     subject: ['', Validators.required],
    //     message: ['', Validators.required],
    // });
    }

}
