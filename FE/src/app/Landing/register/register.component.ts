import { Component, OnInit } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { account } from 'src/app/Models/account';
import { AccountService } from 'src/app/Services/account.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  fileData: File = null;
  registerForm: FormGroup;
  submitted = false;

  constructor(
    private http: HttpClient,
    private accountServ: AccountService,
    private router: Router,
    private formBuilder: FormBuilder,
    ) { }

  //function for loading a profile image
  fileProgress(fileInput: any) 
  {
    if (fileInput.target.files.length > 0)
    { 
    this.fileData = <File>fileInput.target.files[0];
    this.accountServ.currentAccount.image=this.fileData;
    }
  }

  //function for registeration
  onSubmit()
  {
    console.log(this.accountServ.currentAccount);
        this.accountServ.register().subscribe(
      data => {
      //  console.log("Registration successful");
      alert('success');
       this.router.navigate(['/LandingTagger']);
    },
      error => {
        alert(error);
  });
}

    //////////fot the profile image
    // const formData = new FormData();
    // formData.append('file', this.fileData);
    // this.http.post('url/to/your/api', formData)
    //   .subscribe(res => {
    //     console.log(res);
    //     alert('SUCCESS !!');
    //   })

    ngOnInit():void
    {
      this.registerForm = this.formBuilder.group({
        name: ['', Validators.required],
        email: ['', Validators.required,Validators.email],
        password: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9]*'),Validators.minLength(6)]]
    });
    }
  }
 

  

 


