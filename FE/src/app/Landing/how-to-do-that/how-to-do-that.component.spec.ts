import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HowToDoThatComponent } from './how-to-do-that.component';

describe('HowToDoThatComponent', () => {
  let component: HowToDoThatComponent;
  let fixture: ComponentFixture<HowToDoThatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HowToDoThatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HowToDoThatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
