import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { account } from 'src/app/Models/account';
import { AccountService } from 'src/app/Services/account.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


ErrorMsg: string = null;
constructor(private router:Router,private accountServ:AccountService) { }

// function for login
onSubmit()
{
  console.log('login');
  this.ErrorMsg = null;
  this.accountServ.login().subscribe(
    data => {
     console.log("login successful",data);
     //it's not the best way to do that...
     this.accountServ.currentAccount=data;
     if(data.rule=="admin")
     this.router.navigate(['/LandingAdmin']);
     else
     this.router.navigate(['/LandingTagger']);
  },
    error => {
      //console.log("can't login");
      //  this.loading = false;
      this.ErrorMsg = error.Message;
});
}

  ngOnInit() {
  }

}
