import { Component, OnInit } from '@angular/core';
import { Car } from 'src/app/car';

@Component({
  selector: 'app-manage-files',
  templateUrl: './manage-files.component.html',
  styleUrls: ['./manage-files.component.scss']
})
export class ManageFilesComponent implements OnInit {
  cars: Car[];
  cols: any[];

  constructor() { }

  ngOnInit() {
    this.cars=[
      new Car("bbhb","xxx","vvv","c")
    ]
    this.cols = [
      { field: 'vin', header: 'Vin' },
      { field: 'year', header: 'Year' },
      { field: 'brand', header: 'Brand' },
      { field: 'color', header: 'Color' }
  ];
  }

}
