import { Component, OnInit } from '@angular/core';
import { AdminService } from 'src/app/Services/admin.service';
import { account } from 'src/app/Models/account';
// import { Ng2SmartTableModule } from 'ng2-smart-table';


@Component({
 
  selector: 'app-manage-taggers',
  // template:`<ng2-smart-table [settings]="settings"></ng2-smart-table>`,
  templateUrl: './manage-taggers.component.html',
  styleUrls: ['./manage-taggers.component.scss']

})
export class ManageTaggersComponent implements OnInit {
  
  listTaggers:Array<account>=new Array<account>();

  // settings = {
  //   columns: {
  //     id: {
  //       title: 'ID'
  //     },
  //     name: {
  //       title: 'Full Name'
  //     },
  //     username: {
  //       title: 'User Name'
  //     },
  //     email: {
  //       title: 'Email'
  //     }
  //   }
  // };

  constructor(private adminServ:AdminService ) { }

  ngOnInit() {
    // this.getAllTaggers();
  }


  // getAllTaggers() {
  //  this.adminServ.getAllTaggers().subscribe(
  //   data=>{
  //     this.listTaggers=data;
  //     console.log("success get list")},
  //   err=>{console.log("not success to get the taggers");}
  //  )
  // }

  // removeAccount(id:any) {
  //   this.adminServ.removeAccount(id).subscribe(
  //    data=>{
  //      console.log("success remove tagger")},
  //    err=>{console.log("not success to remove tagger");}
  //   )
  //  }
 
}
