import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageTaggersComponent } from './manage-taggers.component';

describe('ManageTaggersComponent', () => {
  let component: ManageTaggersComponent;
  let fixture: ComponentFixture<ManageTaggersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageTaggersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageTaggersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
