import { Component, OnInit } from '@angular/core';
import { TagFileService } from 'src/app/Services/tag-file.service';


@Component({
  selector: 'app-admin-entrance',
  templateUrl: './admin-entrance.component.html',
  styleUrls: ['./admin-entrance.component.scss']
})
export class AdminEntranceComponent implements OnInit {

  fileToUpload: File = null;

  constructor(private TagFileServ:TagFileService) { }

handleFileInput(files: FileList)
{
    this.fileToUpload = files.item(0);
    this.uploadFileToActivity();
}


uploadFileToActivity() {
  this.TagFileServ.postFile(this.fileToUpload).subscribe(data => {
    // do something, if upload success
    }, error => {
      console.log(error);
    });
}
  ngOnInit() {

 

  }

}
