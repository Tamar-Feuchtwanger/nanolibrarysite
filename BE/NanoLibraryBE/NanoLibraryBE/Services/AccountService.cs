﻿using NanoLibraryBE.DTO;
using NanoLibraryBE.EF;
using NanoLibraryBE.Repos;
using NanoLibraryBE.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
namespace NanoLibraryBE.Services
{
    public interface IAccountService
    {
        Account AddUser(Account user);
        Account getUser(Account account);
        List<Account> getAllUsers();
        bool removeAccount(int id);
        String randomPassword();
        Account resetPassword(string email);
        bool updatePassword(Account account);
    }

    public class AccountService : IAccountService
    {
        private int level = 1;
        //the function gets a user and add the user to the db - register.
        public Account AddUser(Account account)
        {
            var repo = new AccountRepo();
            if (repo.GetAllAccounts().Any(a => a.Email == account.Email))
            {
            throw new Exception($"{account.Email} is already exists in account table");
            }
            account.RegisterDate = DateTime.Today;
            account.Leval = level;
            //var account = AutoMapper.Mapper.Map<AccountDTO, Account>(accountDTO);
            var newAccount = repo.Insert(account);
            //return AutoMapper.Mapper.Map<Account, AccountDTO>(newAccount);
            return newAccount;
        }

        public Account getUser(Account account)
        {
            var repo = new AccountRepo();
            var currentAccount = repo.GetByEmail(account.Email);
            if (currentAccount == null)
            {
                throw new Exception($"{account.Email} is not exists");
            }
            if (currentAccount.Password != account.Password)
            {
                throw new Exception($"{account.Password} invalid");
            }
            return currentAccount;

        }

        public List<Account> getAllUsers()
        {
            var repo = new AccountRepo();
            var currentAccountList = repo.GetAllAccounts();
            return currentAccountList.ToList();
        }

        public bool removeAccount(int id)
        {
            var repo = new AccountRepo();
            return repo.removeAcccount(id);
        }
    
        public String randomPassword()
        {
            string password = Guid.NewGuid().ToString("N").ToLower()
                       .Replace("1", "").Replace("o", "").Replace("0", "")
                       .Substring(0, 6);
            return password;
        }

        public Account resetPassword(string email)
        {
            var repo = new AccountRepo();
            return repo.changePassword(email, randomPassword());
        }

        public bool updatePassword(Account account)
        {
            var repo = new AccountRepo();
            return repo.updatePassword(account);
        }
      

    }
}