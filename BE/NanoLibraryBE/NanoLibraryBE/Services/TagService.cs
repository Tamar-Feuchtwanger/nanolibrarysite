﻿using NanoLibraryBE.DTO;
using NanoLibraryBE.EF;
using NanoLibraryBE.Repos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NanoLibraryBE.Services
{

    public interface ITagService
    {
        List<TagProperty> getTagProperyList();
        void createNewTag(DTO.Tag tag);
    }
    public class TagService:ITagService
    {
        public List<TagProperty> getTagProperyList()
        {
        var repo = new TagRepo();
        var currentTagPropertyList = repo.GetAllTagProperties();
        return currentTagPropertyList.ToList();
        }

        public void createNewTag(DTO.Tag tag)
        {
            try
            {
                
            }
            catch
            {
           
            }
            return;
        }

    }
}