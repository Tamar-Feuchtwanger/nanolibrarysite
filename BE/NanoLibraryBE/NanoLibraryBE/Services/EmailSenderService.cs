﻿using NanoLibraryBE.EF;
using NanoLibraryBE.Settings;
using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;


namespace NanoLibraryBE.Services
{
    public interface IEmailSenderService
    {
        Task SendEmailAsync(ContactUs contactUs);
        Task Execute(ContactUs contactUs);
        Task SendEmailPasswordAsync(Account account);
        Task sendEmailWithPasssword(Account account);
    }
    /// <summary>
    /// service implematation for sending emails
    /// </summary>
    //public class EmailSenderService : IEmailSenderService
    //{

        //public EmailSender(IOptions<EmailSettings> emailSettings)
        //{
        //    _emailSettings = emailSettings.Value;
        //}

        //public EmailSettings _emailSettings { get; }

            //new in comment
        //public Task SendEmailAsync(ContactUs contactUs)
        //{
        //    Execute(contactUs).Wait();
        //    return Task.FromResult(0);
        //}

       //new in comment
        //    public Task Execute(ContactUs contactUs)
        //{
        //    try
        //    {
        //        MailMessage mail = new MailMessage();
        //        //from
        //        mail.From = new MailAddress("thirdderivativesoftware@gmail.com", "NLT");
        //        //recipient address
        //        mail.To.Add(new MailAddress(contactUs.emailAddress));
        //        SmtpClient SmtpServer = new SmtpClient();
        //        SmtpServer.Port = 587;
        //        SmtpServer.EnableSsl = true;
        //        SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
        //        SmtpServer.UseDefaultCredentials = false;
        //        SmtpServer.Credentials = new NetworkCredential("thirdderivativesoftware@gmail.com", "akuaAKUA3");
        //        SmtpServer.Host = "smtp.gmail.com";
        //        mail.Subject = contactUs.subject;
        //        //mail.Attachments.Add(new Attachment(@"C:\Users\Public\TestFolder\ToSend.txt"));
        //        mail.IsBodyHtml = true;
        //        mail.Body = contactUs.message;
        //        SmtpServer.Send(mail);
        //        mail.To.Clear();
        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //}

        //public Task SendEmailPasswordAsync(Account account)
        //{
        //   sendEmailWithPasssword(account);
        //    return Task.FromResult(0);
        //}
        //public Task sendEmailWithPasssword(Account account)
        //{
        //    try
        //    {
        //        MailMessage mail = new MailMessage();
        //        //from
        //        mail.From = new MailAddress("thirdderivativesoftware@gmail.com", "NLT");
        //        //recipient address
        //        mail.To.Add(new MailAddress(account.Email));
        //        SmtpClient SmtpServer = new SmtpClient();
        //        SmtpServer.Port = 587;
        //        SmtpServer.EnableSsl = true;
        //        SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
        //        SmtpServer.UseDefaultCredentials = false;
        //        SmtpServer.Credentials = new NetworkCredential("thirdderivativesoftware@gmail.com", "akuaAKUA3");
        //        SmtpServer.Host = "smtp.gmail.com";
        //        mail.Subject = "Your new password:)";
        //        //mail.Attachments.Add(new Attachment(@"C:\Users\Public\TestFolder\ToSend.txt"));
        //        mail.IsBodyHtml = true;
        //        mail.Body = account.Password;
        //        SmtpServer.Send(mail);
        //        mail.To.Clear();
        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //    return null;
        //}


        ////TODO: change it to:  https://benjii.me/2017/02/send-email-using-asp-net-core/
        //public static string Personolize(string data, Models.ApplicationUser user)
        //{
        //    return data.Replace("{name}", user.name)
        //        .Replace("{email}", user.Email).Replace("{title}", user.title).Replace("{institude}", user.institude);
        //}
   // }
}
