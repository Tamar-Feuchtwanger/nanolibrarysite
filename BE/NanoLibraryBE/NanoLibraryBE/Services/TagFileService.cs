﻿
using NanoLibraryBE.common;
using NanoLibraryBE.DTO;
using NanoLibraryBE.EF;
using NanoLibraryBE.Repos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NanoLibraryBE.Services
{
    public interface ITagFileService
    {
        //tagFile GetFile();
        FileDTO GetFile();
        bool AddFile(string fileName, string text);

    }
    public class TagFileService : ITagFileService
    {
        //public tagFile GetFile()
        //{
        //    var repo = new TagFileRepo();
        //    return repo.GetFile();
        //}

        public FileDTO GetFile()
        {
            var repo = new TagFileRepo();
            return repo.GetFile();
        }
        public bool AddFile(string fileName, string text)
        {
            var repo = new TagFileRepo();
            //if (repo.GetAllFiles().Any(a => a.Email == account.Email))
            //{
            //    throw new Exception($"{account.Email} is already exists in account table");
            //}
            var file = new TagFile();
            file.FileName = fileName;
            file.Text = text;
            file.Status = (int)FileStatusEnum.New;
            file.UploadDate = DateTime.Today;
            return repo.Insert(file);

        }
    }
}