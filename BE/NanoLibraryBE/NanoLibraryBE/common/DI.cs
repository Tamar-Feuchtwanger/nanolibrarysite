﻿using Castle.Windsor;

namespace NanoLibraryBE.Common
{
    public class DI
    {
        private static readonly WindsorContainer Container = new WindsorContainer();

        public static WindsorContainer Instance => Container;
    }
}