﻿using Castle.MicroKernel.Registration;
using NanoLibraryBE.Common;
using NanoLibraryBE.Services;
using static NanoLibraryBE.Services.TagService;

namespace NanoLibraryBE.App_Start
{
    public static class DIConfig
    {
        public static void Configure()
        {
            //log4net.Config.XmlConfigurator.Configure();
            //var logger = new LoggerOnLog4Net(log4net.LogManager.GetLogger("Main"));
            //DI.Instance.Register(Component.For<ILogger>().Instance(logger));

            DI.Instance.Register(Component.For(typeof(IAccountService))
           .ImplementedBy(typeof(AccountService)).LifestyleTransient());
           // DI.Instance.Register(Component.For(typeof(IEmailSenderService))
           //.ImplementedBy(typeof(EmailSenderService)).LifestyleTransient());
            DI.Instance.Register(Component.For(typeof(ITagFileService))
           .ImplementedBy(typeof(TagFileService)).LifestyleTransient());
            DI.Instance.Register(Component.For(typeof(ITagService))
           .ImplementedBy(typeof(TagService)).LifestyleTransient());
        }
    }
}
