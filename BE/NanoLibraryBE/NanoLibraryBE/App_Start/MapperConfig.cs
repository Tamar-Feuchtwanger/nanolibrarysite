﻿using AutoMapper;
using NanoLibraryBE.DTO;
using NanoLibraryBE.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NanoLibraryBE.App_Start
{
    public class MapperConfig
    {

        public static void CreateMap()
        {
            Mapper.Initialize(cfg =>
                {
                    //cfg.CreateMap<Account, AccountDTO>()
                    //.ForMember(dest => dest.id, opt => opt.MapFrom(src => src.Id));

                    cfg.CreateMap<TagFile, FileDTO>()
                     .ForMember(dest => dest.StatusName, opt => opt.MapFrom(src => src.FileStatus.StatusName));
                    ;
                });
        }

    }
}
