﻿﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NanoLibraryBE.Settings
{
    public class ContactUs
    {
       Why actually we need these attributes, we have to restrict the form in the angular...
        //[Key]
        //public int ID { get; set; }

        [StringLength(50)]
        public string name { get; set; }

        //[EmailAddress]
        [StringLength(100)]
        public string emailAddress { get; set; }

        //To avoid hard code,
        //meybe it is better to write it in an external file...?
        [EmailAddress]
        [StringLength(100)]
        public string adminEmailAddress { get; set; }

        //[EmailAddress]
        //[StringLength(100)]
        //[Display(Name = "return email")]
        //public string ReturnEmail { get; set; }

        public String PrimaryDomain { get; set; }

        public int PrimaryPort { get; set; }

        [StringLength(100)]
        public string subject { get; set; }

        [StringLength(1000)]
        public string message { get; set; }

        //[StringLength(100)]
        //public string Time { get; set; }
        ContactUs() {
            this.adminEmailAddress = "nanolibrarytagging @gmail.com";
        }

    }
}