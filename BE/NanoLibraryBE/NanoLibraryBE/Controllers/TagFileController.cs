﻿using NanoLibraryBE.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NanoLibraryBE.EF;
using NanoLibraryBE.Services;
using NanoLibraryBE.Common;
using AutoMapper;
using System.Web;
using System.Configuration;
using System.IO;

namespace NanoLibraryBE.Controllers
{
    [RoutePrefix("api/TagFile")]
    public class TagFileController : ApiController
    {
       
        
            private readonly ITagFileService _tagFileService;
            public TagFileController()
            {
                //_emailSender = DI.Instance.Resolve<IEmailSender>();
                //_mapper = mapper;
                _tagFileService = DI.Instance.Resolve<ITagFileService>();
            }

        [HttpGet]
        [Route("getFile")]
        public HttpResponseMessage Get()
            {
                var newFile = _tagFileService.GetFile();
                return Request.CreateResponse(HttpStatusCode.OK, newFile);
                //return Ok(newFile);
            }
        [HttpPost]
        public IHttpActionResult Post()
        {
            if (HttpContext.Current.Request.Files.Count > 0)
            {
                HttpPostedFile file = HttpContext.Current.Request.Files[0];
                string filePath = @ConfigurationManager.AppSettings["ProgramDataFolder"] + file.FileName;
                file.SaveAs(filePath);
                string text = File.ReadAllText(filePath);
                var newFile = _tagFileService.AddFile(file.FileName, text);
                //save....
                return Ok<HttpResponseMessage>(null);
            }
            return BadRequest();
        }


    }
}
