﻿using NanoLibraryBE.Common;
using NanoLibraryBE.DTO;
using NanoLibraryBE.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NanoLibraryBE.EF;
using AutoMapper;

namespace NanoLibraryBE.Controllers
{
[RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private readonly IAccountService _accountService;
        private readonly IEmailSenderService _emailSenderService;
        private readonly IMapper _mapper;

        public AccountController()
        {
            //_mapper = mapper;/
            _accountService = DI.Instance.Resolve<IAccountService>();
            //_emailSenderService = DI.Instance.Resolve<IEmailSenderService>();
        }
     
        [HttpPost]
        [Route("register")]
        public HttpResponseMessage register(Account account)
        {
            var newAccount = _accountService.AddUser(account);
            //account = _mapper.Map<Account>(account);
            return Request.CreateResponse(HttpStatusCode.OK, newAccount);
        }

        [HttpPut]
        [Route("login")]
        public HttpResponseMessage login(Account account)
        {
            var currentAccount = _accountService.getUser(account);
            //account = _mapper.Map<Account>(account);
            return Request.CreateResponse(HttpStatusCode.OK, currentAccount);
            //throw new Exception();
        }

        //[HttpPut]
        //[Route("ForgottenPassword")]
        //public IHttpActionResult ForgottenPassword(/*Account account*/string email)
        //{
        //    var newAcount = _accountService.resetPassword(/*account.*/email);
        //    //account = _mapper.Map<Account>(account);
        //    _emailSenderService.SendEmailPasswordAsync(newAcount);
        //    return Ok(newAcount);
        //    //throw new Exception();
        //}
        


    }
}
