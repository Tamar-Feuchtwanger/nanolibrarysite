﻿using NanoLibraryBE.Common;
using NanoLibraryBE.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NanoLibraryBE.Controllers
{
    [RoutePrefix("api/Admin")]
    public class AdminController : ApiController
    {
        private readonly IAccountService _accountService;
        public AdminController()
        {
            _accountService = DI.Instance.Resolve<IAccountService>();
        }

        [HttpGet]
        [Route("getAllAccounts")]
        public HttpResponseMessage getAllAccounts()
        {
            var newListAccounts = _accountService.getAllUsers();
           
            return Request.CreateResponse(HttpStatusCode.OK, newListAccounts);
        }

        [HttpPut]
        [Route("removeAccount/{id}")]
        public HttpResponseMessage removeAccount(int id)
        {
            var newListAccounts = _accountService.removeAccount(id);
            return Request.CreateResponse(HttpStatusCode.OK, newListAccounts);
        }

    }

}
