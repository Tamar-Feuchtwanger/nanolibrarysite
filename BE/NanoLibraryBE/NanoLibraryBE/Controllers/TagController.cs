﻿using AutoMapper;
using NanoLibraryBE.Common;
using NanoLibraryBE.DTO;
using NanoLibraryBE.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static NanoLibraryBE.Services.TagService;

namespace NanoLibraryBE.Controllers
{
    [RoutePrefix("api/Tag")]
    public class TagController : ApiController
    {
        private readonly ITagService _tagService;
        //private readonly IMapper _mapper;

        public TagController()
        {
        _tagService = DI.Instance.Resolve<ITagService>();
         //_mapper = IMapper;
        }

        [HttpGet]
        [Route("getTagPropertyList")]
        public IHttpActionResult getAllAccounts()
        {
            var newTagPropertyList = _tagService.getTagProperyList();
            return Ok(newTagPropertyList);
        }

        //[HttpPost]
        //[Route("create")]
        //public HttpResponseMessage createNewTag(Tag tag)
        //{
        //    var newTag = _tagService.createNewTag(tag);
        //    return Request.CreateResponse(HttpStatusCode.OK);
        //}


    }
}
