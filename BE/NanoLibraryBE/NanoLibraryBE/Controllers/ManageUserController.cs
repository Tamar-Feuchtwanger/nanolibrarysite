﻿using NanoLibraryBE.Common;
using NanoLibraryBE.EF;
using NanoLibraryBE.Services;
using NanoLibraryBE.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NanoLibraryBE.Controllers
{
    [RoutePrefix("api/ManageUser")]
    public class ManageUserController : ApiController
    {
        private readonly IEmailSenderService _emailSenderService;
        //private readonly IMapper _mapper;

        public ManageUserController()
        {
            _emailSenderService = DI.Instance.Resolve<IEmailSenderService>();
            //_mapper = mapper;
        }

        [HttpPost]
        [Route("contactUs")]
        public HttpResponseMessage contact(ContactUs contactUs)
        {
            var newContact = _emailSenderService.SendEmailAsync(contactUs);
            //account = _mapper.Map<Account>(account);
            return Request.CreateResponse(HttpStatusCode.OK);
        }

    }


}
