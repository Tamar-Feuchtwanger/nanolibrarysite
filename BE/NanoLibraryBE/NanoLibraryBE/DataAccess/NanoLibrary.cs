﻿using NanoLibraryBE.EF;

namespace NanoLibraryBE.Repos
{
    public class NanoLibrary
    {
        public static NanoLibraryDB Create()
        {
            return new NanoLibraryDB("NanoLibraryDB");
        }
    }
}
