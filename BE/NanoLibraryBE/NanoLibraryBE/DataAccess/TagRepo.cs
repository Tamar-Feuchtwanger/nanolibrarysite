﻿using NanoLibraryBE.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NanoLibraryBE.Repos
{
    public class TagRepo
    {
        public TagProperty[] GetAllTagProperties()
        {
            var context = NanoLibrary.Create();
            return context.TagProperties.ToArray();
        }

    }
}