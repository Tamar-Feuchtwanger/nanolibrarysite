﻿
using NanoLibraryBE.DTO;
using NanoLibraryBE.EF;
using System.Linq;
using NanoLibraryBE.common;
using AutoMapper.QueryableExtensions;
using System.Data.Entity;

namespace NanoLibraryBE.Repos
{
    public class TagFileRepo
    {
        public FileDTO GetFile()
        {
            using (var context = new NanoLibraryDB())
            {
                var tagFile = context.TagFiles
                    .Include(x => x.FileStatus)
                    .Where(x => x.Status == (int)FileStatusEnum.New)
                    .ProjectTo<FileDTO>()
                    .OrderBy(w => w.Id).FirstOrDefault();
                return tagFile;
            }

            //if (context.tagFiles.Where(x => x.status == 1).Any())
            //{
            //var tagFile = context.tagFiles/*.Where(x => x.status == 1)*/
            //      .OrderBy(w => w.id).FirstOrDefault();
            //tagFile.status = 2;
            //context.SaveChanges();
            //return tagFile;
            //}
            //return null;

        }

        public bool Insert(TagFile file)
        {
            var context = NanoLibrary.Create();
           // NanoLibraryDB db = new NanoLibraryDB();
            try
            {
                context.TagFiles.Add(file);
                context.SaveChanges();
            }
            catch (System.Exception)
            {
                return false;
            }

            return true;

        }
    }
}