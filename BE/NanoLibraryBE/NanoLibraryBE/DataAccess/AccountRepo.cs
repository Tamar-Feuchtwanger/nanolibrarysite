﻿using NanoLibraryBE.EF;
using System.Linq;

namespace NanoLibraryBE.Repos
{
    public class AccountRepo
    {
        public Account Insert(Account account)
        {
            //var context = NanoLibrary.Create();
            NanoLibraryDB db = new NanoLibraryDB();

            //context.Accounts.Add(account);
            //context.SaveChanges();

            db.Accounts.Add(account);
            db.SaveChanges();
            return account;
        }

        public Account[] GetAllAccounts()
        {
            var context = NanoLibrary.Create();
            return context.Accounts.ToArray();
        }

        public Account GetById(int id)
        {
            var context = NanoLibrary.Create();
            return context.Accounts.Find(id);
        }

        public Account GetByEmail(string email)
        {
            var context = NanoLibrary.Create();
            return context.Accounts.SingleOrDefault(account => account.Email == email);
        }

        public bool removeAcccount(int id)
        {
            var context = NanoLibrary.Create();
            var currentAccount = GetById(id);
            if(currentAccount!=null)
            { 
                context.Accounts.Remove(currentAccount);
                //לא לשכוח לשמור שינויים
                return true;
            }
            return false;
        }

        public Account changePassword(string email, string newPassword)
        {
            var context = NanoLibrary.Create();
            //NanoLibraryDB db = new NanoLibraryDB();
            //לשים לב שהוא מחזיר סיסמא 
            var cuurentAccount =GetByEmail(email);
            cuurentAccount.Password = newPassword;
            context.SaveChanges();
            return cuurentAccount;
        }

        public bool updatePassword(Account account)
        {
            var context = NanoLibrary.Create();
            var currentAccount = GetByEmail(account.Email);
            if (currentAccount != null)
            {
                context.Accounts.Find(currentAccount.Id).Password = account.Password;
                return true;
            }
            return false;
        }

    }
}