﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NanoLibraryBE.DTO
{
    public class Tag
    {
        public short id { get; set; }
        public short relatedFileId { get; set; }
        public short property { get; set; }
        public string value { get; set; }
        public string propertyWord { get; set; }
        public string sentence { get; set; }
        public DateTime date { get; set; }
       
    }
}