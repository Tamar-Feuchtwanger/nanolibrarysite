﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NanoLibraryBE.DTO
{
    public class LibraryRun
    {
        public short id { get; set; }
        public DateTime date { get; set; }
        public string accountId { get; set; }
        public string input1 { get; set; }
        public string input2 { get; set; }
        public string input3 { get; set; }
      
    }
}