﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NanoLibraryBE.DTO
{
    public class TagFileDTO
    {
        public short id { get; set; }
        public string name { get; set; }
        public short fileSizeBytes { get; set; }
        public short status { get; set; }
        public string text { get; set; }
        public double singleTagPrice { get; set; }
        public double filePrice { get; set; }
        public DateTime uploudDate { get; set; }
       
    }
}