﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NanoLibraryBE.DTO
{
    public class AccountChat
    {
        public short id { get; set; }
        public string message { get; set; }
        public DateTime date { get; set; }
        public bool isAdmin { get; set; }
       

    }
}
