﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NanoLibraryBE.DTO
{
    public class Payments
    {
        public short id { get; set; }
        public double amount { get; set; }
        public DateTime date { get; set; }
        public string accountId { get; set; }
        public short adminPaidAccountId { get; set; }
       
    }
}