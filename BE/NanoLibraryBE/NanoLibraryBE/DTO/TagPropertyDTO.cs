﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NanoLibraryBE.DTO
{
    public class TagPropertyDTO
    {
        public short id { get; set; }
        public string name { get; set; }
        public string samples { get; set; }
        public short type { get; set; }
       
      
    }
}