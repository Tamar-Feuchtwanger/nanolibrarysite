﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NanoLibraryBE.DTO
{
    public class FileDTO
    {
       
            public short Id { get; set; } // id (Primary key)
            public string FileName { get; set; } // file_name (length: 100)
            public short? FileSizeBytes { get; set; } // file_size_bytes
            public short Status { get; set; } // status
            public string StatusName { get; set; } 
            public string Text { get; set; } // text (length: 1073741823)
            public double? SingleTagPrice { get; set; } // single_tag_price
            public double? FilePrice { get; set; } // file_price
            public System.DateTime? UploadDate { get; set; } // upload_date

           
        
    }
}