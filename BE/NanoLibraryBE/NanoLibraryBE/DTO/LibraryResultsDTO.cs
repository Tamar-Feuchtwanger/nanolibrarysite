﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace NanoLibraryBE.DTO
{
    public class LibraryResultsDTO
    {
        public short id { get; set; }
        public  Nullable<short> runId { get; set; }
        public string results { get; set; }

     
    }
}